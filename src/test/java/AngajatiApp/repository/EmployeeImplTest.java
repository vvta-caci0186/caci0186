package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.controller.EmployeeController;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeImplTest {

    EmployeeImpl employeeController ;

    @BeforeEach
    void setUp() {
        employeeController = new EmployeeImpl();
    }


    @Test
    void addEmployee_TC1() {
        Employee employee = new Employee(
                "Popescu",
                "Ion",
                "1680508205894",
                DidacticFunction.TEACHER,
                4525.5);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees+1, employeeController.getEmployeeList().size());
        assertTrue(added);
    }

    @Test
    void addEmployee_TC2() {
        Employee employee = new Employee(
                "Popescu",
                "Ion",
                "168ertsdf5894",
                DidacticFunction.TEACHER,
                4525.5);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees, employeeController.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }

    @Test
    void addEmployee_TC3() {
        Employee employee = new Employee(
                "Popescu",
                "Ion",
                "168050820589",
                DidacticFunction.TEACHER,
                4525.5);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees, employeeController.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }

    @Test
    void addEmployee_TC4() {
        Employee employee = new Employee(
                "popescu",
                "Ion",
                "168050820589",
                DidacticFunction.TEACHER,
                4525.5);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees, employeeController.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }

    @Test
    void addEmployee_TC5() {
        Employee employee = new Employee(
                "Popescu",
                "ion",
                "168050820589",
                DidacticFunction.TEACHER,
                4525.5);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees, employeeController.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }

    @Test
    void addEmployee_TC3_BVA() {
        Employee employee = new Employee(
                "Popescu",
                "Ion",
                "16805082058948",
                DidacticFunction.TEACHER,
                4525.5);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees, employeeController.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }

    @Test
    void addEmployee_TC4_BVA() {
        Employee employee = new Employee(
                "@opescu",
                "Ion",
                "16805082058948",
                DidacticFunction.TEACHER,
                4525.5);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees, employeeController.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }

    @Test
    void addEmployee_TC5_BVA() {
        Employee employee = new Employee(
                "Ardelean",
                "Violeta",
                "2980508205894",
                DidacticFunction.LECTURER,
                8575.9);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees+1, employeeController.getEmployeeList().size());
        assertTrue(added);
        System.out.println("TRUE");
    }

    @Test
    void addEmployee_TC6_BVA() {
        Employee employee = new Employee(
                "Zicu",
                "Mihai",
                "1980518205894",
                DidacticFunction.LECTURER,
                8575.9);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees+1, employeeController.getEmployeeList().size());
        assertTrue(added);
        System.out.println("TRUE");
    }

    @Test
    void addEmployee_TC7_BVA() {
        Employee employee = new Employee(
                "[opescu",
                "Ion",
                "16805082058948",
                DidacticFunction.TEACHER,
                4525.5);

        int noOfEmployees = employeeController.getEmployeeList().size();
        boolean added = employeeController.addEmployee(employee);
        assertEquals(noOfEmployees, employeeController.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }












}

