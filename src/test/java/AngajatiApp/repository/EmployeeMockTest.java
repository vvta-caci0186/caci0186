package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    @Test
    void modifyEmployeeFunction_TC01() {
        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();

        Employee employee = employeeList.get(1);

        System.out.println("Initial function was: LECTURER");
        assertEquals(DidacticFunction.LECTURER, employee.getFunction());

        employeeMock.modifyEmployeeFunction(employee, DidacticFunction.ASISTENT);

        System.out.println("Modified function is: ASISTENT");
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());
    }

    @Test
    void modifyEmployeeFunction_TC02() throws EmployeeException {
        EmployeeMock employeeMock = new EmployeeMock(0);
        List<Employee> employeeList = employeeMock.getEmployeeList();

        assertEquals(0, employeeList.size());
    }

    @Test
    void modifyEmployeeFunction_TC03() {
        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();

        employeeMock.modifyEmployeeFunction(null, DidacticFunction.ASISTENT);

        List<Employee> newEmployeeList = employeeMock.getEmployeeList();

        assertEquals(employeeList, newEmployeeList);
        System.out.println("With the null parameter the list remains the same");
    }
}






