package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

class EmployeeTest {
    Employee employee;

    @BeforeEach
    void setUpTest() {
        employee = new Employee(
                "Colhon",
                "Alexandra",
                "2920710014660",
                DidacticFunction.ASISTENT,
                1500.2);
    }

    @Test
    void testConstructorSetsFieldsCorrectly() {
        assertEquals("Colhon", employee.getFirstName());
        assertEquals("Alexandra", employee.getLastName());
        assertEquals("2920710014660", employee.getCnp());
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());
        assertEquals(1500.2, employee.getSalary());
    }

    @Test
    void getId() {
        assertEquals(0, employee.getId());
    }

    @Test
    void setId() {
        employee.setId(2);
        assertEquals(2, employee.getId());
    }

    @Test
    void getFirstName() {
        assertEquals("Colhon", employee.getFirstName());
    }

    @Test
    void setFirstName() {
        employee.setFirstName("Filip");
        assertEquals("Filip", employee.getFirstName());
    }

    @Test
    void getLastName() {
        assertEquals("Alexandra", employee.getLastName());
    }

    @Test
    void setLastName() {
        employee.setLastName("Ciprian");
        assertEquals("Ciprian", employee.getLastName());
    }

    @Test
    void getCnp() {
        assertEquals("2920710014660", employee.getCnp());
    }

    @Test
    void setCnp() {
        employee.setCnp("2700126010322");
        assertEquals("2700126010322", employee.getCnp());
    }

    @Test
    void getFunction() {
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());
    }

    @Test
    void setFunction() {
        employee.setFunction(DidacticFunction.CONFERENTIAR);
        assertEquals(DidacticFunction.CONFERENTIAR, employee.getFunction());
    }

    @Test
    void getSalary() {
        assertEquals(1500.2, employee.getSalary());
    }

    @Test
    void setSalary() {
        employee.setSalary(2500.0);
        assertEquals(2500.0, employee.getSalary());
    }

    @Test
    public void testGetFunctionWithException(){
        Employee employee = null;
        System.out.println("Test that Exception is thrown");
        Assertions.assertThrows(NullPointerException.class, () -> {
            employee.getFunction();
        });
    }

    void delay(int time) {
        try {
            TimeUnit.SECONDS.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testTimeoutGetFirstName() {
        // timpul acordat testului: 10s... timpul dupa care incepe testul 5s
        assertTimeout(Duration.ofSeconds(10), () -> {
            delay(5);
            System.out.println("Timeout test that does not fail");
            employee.getFirstName();
        });
    }

    @ParameterizedTest
    @NullSource
    @Order(3)
    public void testSetCNPNull(String CNP){
        employee.setCnp(CNP);
        System.out.println("Parameterized Test with null source");
        assertNull(employee.getCnp());
    }

    @ParameterizedTest
    @EmptySource
    @Order(2)
    public void testSetFirstNameEmpty(String firstName){
        employee.setFirstName(firstName);
        System.out.println("Parameterized Test with empty source");
        assertEquals("", employee.getFirstName());
    }

    @ParameterizedTest
    @ValueSource(doubles = {1500.5, 2500.5, 3500.5})
    @Order(1)
    public void testSetSalaryWithValue(Double salary) {
        employee.setSalary(salary);
        System.out.println("Parameterized Test with value source: " + salary);
        assertEquals(salary, employee.getSalary());
    }

}